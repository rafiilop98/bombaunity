﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    [Tooltip("bomba que se lanza")]
    public GameObject bomb;
    [Tooltip("Donde se generan las bombas")]
    public Transform spawnPosition;
    [Tooltip("Bombas por segundo")]
    public float bombRate = 0.5f;
    [Tooltip("velocidad de caida de la bomba")]
    public float bombSpeed = 3.0f;
    [Tooltip("velocidad de desplazamiento")]
    public float speed = 3.0f;
    [Tooltip("minima x durante el desplazamiento")]
    public float xMin = -10;
    [Tooltip("Maxima x durante desplazamiento")]
    public float xMax = 10;

    private float currentDir;
    private float nextBombTime;

    // Start is called before the first frame update
    void Start()
    {
        if (bombRate <= 0.0)
        {
            Debug.LogWarning("bombRate debe ser positivo");
            bombRate = 1.0f;
        }
        currentDir = 1.0f;
        randomlyChangeDirection();
        nextBombTime = Time.time + 1.0f / bombRate;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 p = transform.position;
        p.x += speed * currentDir * Time.deltaTime;
        if (p.x < xMin)
        {
            p.x = xMin;
            currentDir = currentDir * -1;
        }
        else if (p.x > xMax)
        {
            p.x = xMax;
            currentDir *= -1;
        }
        transform.position = p;
        if (Time.time >= nextBombTime)
        {
            Bombard();
            randomlyChangeDirection();
            nextBombTime += 1.0f / bombRate;
        }
    }
    void Bombard()
    {
        if (bomb != null && spawnPosition != null)
        {
            GameObject newBomb;
            newBomb = Instantiate(bomb, spawnPosition.position, spawnPosition.rotation);
            bomba bc;
            bc = newBomb.GetComponent<bomba>();
            bc.speed = bombSpeed;

        }
    }
    void randomlyChangeDirection()
    {
        if (Random.value < 0.5)
        {
            currentDir *= -1;
        }
    }
}